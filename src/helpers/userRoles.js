const knex = require('../utils/database')
const first = require('lodash.first')
const lodash = require('lodash')

const addUserRoles = async (userInfo) => {
    const users = await knex('users')
      .select('id')
      .where({'user_id': userInfo.userid, 'server_id': userInfo.guild})
    const user = first(users)
    if (user) {
        var roles = await getDBData(userInfo, user.id)
    } else {
      return false
    }
}

const getDBData = async (userInfo, userId) => {
    userInfo.roles.forEach(async (userRole) => {
        const userRoles = await knex
            .select('roles.role_id', 'users.nickname', 'roles.role_name')
            .from('roles')
            .innerJoin('users', {'users.id': userId})
            .innerJoin('userroles', {'users.id': 'userroles.userrole_user'})
            .where({'roles.role_snowflake': userRole.id, })
        const dbData = first(userRoles)
        if(dbData) {
            return false }
        else {
            await addUserRole(userRole.id, userId)
        }
    })
}

const updateUserRoles = async (userInfo, oldUserInfo) => {
    let roles = []
    let oldRoles = []
    const users = await knex('users')
    .select('id')
    .where({'user_id': userInfo.userid, 'server_id': userInfo.guild})
    const user = first(users)
    userInfo.roles.forEach(async (role) => {
        roles.push(role.id)
    })
    oldUserInfo.roles.forEach(async (oldRole) => {
        oldRoles.push(oldRole.id)
    })
    let addedRoles = lodash.difference(roles, oldRoles)
    let removedRoles = lodash.difference(oldRoles, roles)
    addedRoles.forEach(async (roleId) => {
        await addUserRole(roleId, user.id)
    })
    removedRoles.forEach(async (roleId) => {
        await deleteUserRole(roleId, user.id)
    })
    
}

const addUserRole = async (userRole, userId) => {
    const roles = await knex
      .select('role_id')
      .from('roles')
      .where({'role_snowflake': userRole})
    const role = first(roles)
    if (role) {
      await knex('userroles')
        .insert({
            'userrole_role': role.role_id,
            'userrole_user': userId
        })
    }
    else {
        console.log('Something went wrong, role not found in DB')
    }
}

const deleteUserRole = async (userRole, userId) => {
    const roles = await knex
      .select('role_id')
      .from('roles')
      .where({'role_snowflake': userRole})
    const role = first(roles)
    if (role) {
      await knex('userroles')
      .where({
        'userrole_role': role.role_id,
        'userrole_user': userId
      })
      .del()
    } else {
      return false
    }
}

module.exports = { addUserRoles, updateUserRoles }