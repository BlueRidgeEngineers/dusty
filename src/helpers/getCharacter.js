const first = require('lodash.first')
const knex = require('../utils/database')

const getCharacter = async (designation) => {
    const character = await getCharacterFromDataBase(designation)
    return character
}

const getCharacterFromDataBase = async (designation) => {
    const character = await knex.select('char_id')
        .from('web_characters')
        .where({'char_designation': designation})
        .pluck('char_id')
    return parseInt(character)
}

module.exports = getCharacter