const knex = require('../utils/database')
const first = require('lodash.first')
var Crypto = require('crypto')

const generatePassword = async (msg) => {
    const users = await knex.select('*').from('users').where({
        'user_id': msg.author.id,
        'server_id': msg.guild.id
      })
    const user = first(users) 
    const tempPassword = await createPassword()
    const salt = await getSalt(user)
    const password = await encodePassword(tempPassword, salt)
    await savePassword(password, salt, msg.author.id, msg.guild.id)
    msg.author.send(`Here's your new password, be sure to change it immediatly: ${tempPassword}`)
  }

const createPassword = async (tempPassword) => {
    tempPassword = tempPassword || ""
    var random = Math.floor((Math.random() * wordTable.length) + 1);
    if(tempPassword.length <= 19) {
        tempPassword = tempPassword + wordTable[random]+"."
        return await createPassword(tempPassword)
    }
    else if(tempPassword.length > 19) {
        tempPassword = tempPassword + wordTable[random]  
    }
    return tempPassword
}

const getSalt = async (user) => {
    if(user.salt ==null) {
        var salt = Crypto.randomBytes(20).toString('hex')
        return salt
    }
    else return user.salt
}

const encodePassword = async (password, salt) => {
    var passhash = Buffer.from(password+salt).toString('base64')
    var hash = Crypto.createHash('sha256')
    hash.update(passhash)
    return hash.digest('hex')
}

const savePassword = async (password, salt, userid, guild) => {
    await knex('users')
        .where({
            'user_id': userid,
            'server_id': guild
        })
        .update({
            'salt': salt,
            'password': password,
            'userset': false
        })
}

const wordTable = [
"precede",
"distance",
"wilderness",
"incredible",
"youthful",
"stay",
"tent",
"market",
"unbecoming",
"offend",
"super",
"hurry",
"hungry",
"bite-sized",
"multiply",
"file",
"plane",
"scare",
"man",
"plant",
"decorate",
"pan",
"organic",
"rhythm",
"abundant",
"brick",
"draconian",
"clammy",
"better",
"drink",
"spring",
"advertisement",
"approve",
"great",
"limping",
"heavenly",
"sand",
"moor",
"jar",
"neighborly",
"competition",
"hope",
"disagreeable",
"hour",
"efficacious",
"roll",
"profit",
"old-fashioned",
"haunt",
"train",
"dislike",
"insurance",
"exuberant",
"balance",
"destroy",
"group",
"turkey",
"drum",
"educated",
"observe",
"slap",
"girl",
"cooperative",
"flesh",
"tremble",
"glass",
"pick",
"angle",
"juvenile",
"vague",
"hobbies",
"harbor",
"pie",
"excited",
"knife",
"quizzical",
"behavior",
"empty",
"smell",
"cultured",
"rinse",
"bent",
"van",
"desert",
"men",
"plucky",
"cows",
"resolute",
"wrestle",
"smooth",
"point",
"responsible",
"voyage",
"chickens",
"petite",
"society",
"smoke",
"label",
"thundering",
"expand",
"awful",
"collar",
"gray",
"mundane",
"suspect",
"three",
"tank",
"glossy",
"heat",
"valuable",
"queue",
"celery",
"past",
"face",
"warlike",
"creature",
"nest",
"alleged",
"abusive",
"unequal",
"cough",
"fact",
"placid",
"flight",
"check",
"defeated",
"squeamish",
"guitar",
"matter",
"fold",
"class",
"friendly",
"avoid",
"merciful",
"stamp",
"temper",
"knowing",
"soak",
"increase",
"breathe",
"help",
"profuse",
"pigs",
"trashy",
"tooth",
"try",
"sail",
"energetic",
"memorise",
"happen",
"snails",
"religion",
"wet",
"sweater",
"fear",
"rabbits",
"hook",
"wonder",
"parcel",
"education",
"teeny",
"press",
"fancy",
"ear",
"drain",
"opposite",
"authority",
"trace",
"silent",
"unhealthy",
"trail",
"gabby",
"fire",
"ask",
"fang",
"tongue",
"orange",
"walk",
"sound",
"kettle",
"optimal",
"obscene",
"teeth",
"use",
"gleaming",
"fork",
"dress",
"vast",
"play",
"bashful",
"street",
"exercise",
"pack",
"elite",
"ratty",
"testy",
"toothbrush",
"square",
"sugar",
"book",
"bike",
"grouchy",
"gullible",
"oafish",
"interesting",
"gun",
"describe",
"unwritten",
"laborer",
"quarter",
"rice",
"questionable",
"lopsided",
"learned",
"disapprove",
"weigh",
"inconclusive",
"evanescent",
"enchanting",
"room",
"laugh",
"parched",
"spray",
"new",
"rose",
"miss",
"suit",
"cherry",
"question",
"imminent",
"like",
"vacation",
"squirrel",
"loutish",
"tart",
"pat",
"touch",
"frantic",
"stare",
"endurable",
"thumb",
"close",
"plain",
"spooky",
"deafening",
"befitting",
"dogs",
"impartial",
"cross",
"poison",
"glue",
"attach",
"sleepy",
"steel",
"fine",
"air",
"jelly",
"pancake",
"calculator",
"price",
"invite",
"pies",
"march",
"chalk",
"lively",
"size",
"x-ray",
"abnormal",
"frog",
"account",
"regret",
"certain",
"murky",
"freezing",
"irritating",
"deeply",
"cellar",
"tawdry",
"confess",
"grey",
"strap",
"uptight",
"boring",
"children",
"sink",
"glistening",
"achiever",
"prickly",
"scarce",
"sleep",
"trade",
"animated",
"knowledge",
"present",
"oceanic",
"arrange",
"oil",
"reduce",
"homeless",
"nappy",
"airport",
"chess",
"design",
"horrible",
"hate",
"strange",
"tug",
"meat",
"trip",
"hand",
"boil",
"need",
"fresh",
"deranged",
"post",
"car",
"premium",
"stereotyped",
"damage",
"berserk",
"visitor",
"jam",
"whirl",
"pointless",
"stream",
"home",
"wrong",
"hole",
"rejoice",
"peace",
"can",
"torpid",
"wash",
"direful",
"rural",
"sigh",
"sparkling",
"fairies",
"muddled",
"coach",
"note",
"unarmed",
"whistle",
"uppity",
"splendid",
"combative",
"silent",
"abiding",
"chicken",
"zippy",
"lyrical",
"spell",
"abaft",
"truck",
"receipt",
"wind",
"town",
"tramp",
"thank",
"elegant",
"cub",
"successful",
"illustrious",
"ubiquitous",
"savory",
"bridge",
"quiet",
"green",
"long",
"crayon",
"volcano",
"odd",
"quarrelsome",
"tender",
"gruesome",
"sheet",
"silky",
"quaint",
"nine",
"wretched",
"peck",
"untidy",
"guard",
"enormous",
"stop",
"neck",
"righteous",
"jumbled",
"disastrous",
"fallacious",
"dear",
"succinct",
"baby",
"join",
"entertain",
"concern",
"friend",
"synonymous",
"clap",
"waiting",
"amused",
"advise",
"satisfy",
"dapper",
"wire",
"yellow",
"tired",
"sweet",
"story",
"things",
"thoughtful",
"tranquil",
"prepare",
"shape",
"steady",
"representative",
"fumbling",
"spiffy",
"flavor",
"reign",
"hurt",
"stale",
"match",
"buzz",
"gentle",
"woebegone",
"practise",
"plants",
"telling",
"puffy",
"callous",
"fire",
"cause",
"ruin",
"pickle",
"skillful",
"momentous",
"crook",
"shiny",
"apathetic",
"obsolete",
"handle",
"colossal",
"jumpy",
"insect",
"fuzzy",
"cap",
"heap",
"trains",
"hushed",
"preach",
"yawn",
"tumble",
"permit",
"squeak",
"awesome",
"force",
"glib",
"frightened",
"sophisticated",
"clean",
"discovery",
"present",
"planes",
"quiver",
"truculent",
"yarn",
"physical",
"camp",
"real",
"thirsty",
"true",
"curl",
"lowly",
"summer",
"minister",
"flap",
"company",
"sneaky",
"expensive",
"warm",
"verdant",
"shoe",
"minute",
"belong",
"letters",
"bare",
"comfortable",
"attack",
"selfish",
"encouraging",
"eight",
"handsomely",
"carve",
"vagabond",
"sincere",
"hot",
"comparison",
"coach",
"bear",
"equal",
"middle",
"kindhearted",
"puzzling",
"screeching",
"innocent",
"perfect",
"servant",
"wonderful",
"land",
"tough",
"boundless"]
  
  module.exports = generatePassword