const first = require('lodash.first')
const knex = require('../utils/database')
const getDBMember = require('./getDBMember')

const saveBotCommandToDB = async (author, message) => {
    const dbMember = await getDBMember(author)
    if(dbMember)
    {
    await knex('bot_commands').insert({
        'command_author': dbMember,
        'command_message': message,
      })
    }
}

const getBotCommandAuthor = async (message) => {
    const members = await knex.select('command_author')
        .from('bot_commands')
        .where({'command_message': message})
        .pluck('command_author')
    if(members) {
        return first(members)
    }
    else {
        return false
    }
}

const getBotCommandDBEntry = async (message) => {
    const members = await knex.select('command_id')
        .from('bot_commands')
        .where({'command_message': message})
        .pluck('command_id')
    if(members) {
        return first(members)
    }
    else {
        return false
    }
}

const deleteBotCommandFromDB = async (message) => {
    await knex('bot_commands')
    .where({
        'command_message': message
    })
    .del()
}

module.exports = {
    saveBotCommandToDB,
    deleteBotCommandFromDB,
    getBotCommandAuthor,
    getBotCommandDBEntry
}
