const getDBMember = require('./getDBMember')
const knex = require('../utils/database')
const { rpChannels } = require('../constants')

const addRpXpToCharacter = async (message, channelId, userId) => {
    if(rpChannels.includes(channelId)) {
        const member = await getDBMember(userId)
        let character = await knex.first('char_discordxp', 'char_id', 'char_lastmsg', 'char_name', 'char_lastname').from('web_characters').where({
            'char_user': member,
            'char_activechar': 1
        })
        if(character != undefined) {
            if ((new Date() - new Date(character.char_lastmsg)) < (60000 * 3)) return
            const newXP = await calculateXP(message)
            const calculatedDate = await setLastMessageDate(newXP, character.char_lastmsg)
            console.log(`Char ${character.char_name} ${character.char_lastname} earned ${newXP} for their message`)
            await knex
            .update({'char_discordxp': character.char_discordxp + 5, 'char_lastmsg': calculatedDate})
            .from('web_characters')
            .where({'char_id': character.char_id})
        }
    }
    return true
}

const calculateXP = async (message) => {
    const maxlength = 500
    var length = 0
    var XP = 0
    if(message.length > maxlength) {
        length = maxlength
    }
    else {
        length = message.length
    }
    XP = Math.floor((length/50))
    return XP
}

const setLastMessageDate = async (XP, dbDate) => {
    var returnDate
    if(XP == 0) {
        returnDate = dbDate
    }
    else {
        returnDate = new Date()
    }
    return returnDate
}

module.exports = addRpXpToCharacter