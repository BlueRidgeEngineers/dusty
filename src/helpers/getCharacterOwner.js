const first = require('lodash.first')
const knex = require('../utils/database')

const getCharacterOwner = async (characterId) => {
    const owner = await getCharacterOwnerFromDataBase(characterId)
    return owner
}

const getCharacterOwnerFromDataBase = async (characterId) => {
    const owner = await knex.select('char_user')
        .from('web_characters')
        .where({'char_id': characterId})
        .pluck('char_user')
    return parseInt(owner)
}

module.exports = getCharacterOwner