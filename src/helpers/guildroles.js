const knex = require('../utils/database')
const first = require('lodash.first')

const addRole = async (roleInfo) => {
    const roles = await knex
      .select('role_snowflake')
      .from('roles')
      .where({'role_snowflake': roleInfo.snowflake, 'role_guild': roleInfo.guild})
    const role = first(roles)
    if (role) {
      await updateRole(roleInfo)
    } else {
      await knex('roles')
        .insert({
            'role_snowflake': roleInfo.snowflake,
            'role_name': roleInfo.name,
            'role_guild': roleInfo.guild
        })
    }
}

const updateRole = async (roleInfo) => {
    const roles = await knex
      .select('role_snowflake')
      .from('roles')
      .where({'role_snowflake': roleInfo.snowflake, 'role_guild': roleInfo.guild})
    const role = first(roles)
    if (role) {
      await knex('roles')
      .where({
        'role_snowflake': roleInfo.snowflake,
        'role_guild': roleInfo.guild
      })
      .update({
        'role_snowflake': roleInfo.snowflake,
        'role_name': roleInfo.name,
        'role_guild': roleInfo.guild
      })
    } else {
      return false
    }
}

const deleteRole = async (roleInfo) => {
    const roles = await knex
      .select('role_snowflake')
      .from('roles')
      .where({'role_snowflake': roleInfo.snowflake, 'role_guild': roleInfo.guild})
    const role = first(roles)
    if (role) {
      await knex('roles')
      .where({
        'role_snowflake': roleInfo.snowflake,
        'role_guild': roleInfo.guild
      })
      .del()
    } else {
      return false
    }
}

module.exports = {
    addRole,
    deleteRole,
    updateRole
  }
  