//vars for functions
//var today = new Date();
//var dd = today.getDate();
//var mm = today.getMonth()+1; //January is 0!
//var yyyy = today.getFullYear();
//var hh = today.getHours();
//var minutes = today.getMinutes();
//var ss = today.getSeconds(); 
var today = ""
var dd = ""
var mm = ""
var yyyy = ""
var hh = ""
var minutes = ""
var ss = ""

const months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];

const setting_time = () => {
    today = new Date();
    dd = today.getDate();
    mm = today.getMonth()+1; //January is 0!
    yyyy = today.getFullYear();
    hh = today.getHours();
    minutes = today.getMinutes();
    ss = today.getSeconds(); 
}

module.exports = {
    now: function() {
        setting_time();
        return `${yyyy}-${mm}-${dd}`;
    },

    date: function () {
        today = ""
        setting_time()
        
        if(dd<10) {
            dd = '0'+dd
        } 
        
        if(mm<10) {
             mm = '0'+mm
        } 
        
        today = mm + dd + yyyy;
        
        return today;
    },

    time: function() {
        today = ""
        setting_time()

        today = hh + ":" + minutes + ":" + ss
        return today;
    },

    dateAndTime: function() {
        today = ""
        setting_time()

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 

        //today = hh + ":" + minutes + ":" + ss + "" + mm + dd + yyyy;
        today = `${hh}:${minutes}:${ss} - ${mm}${dd}${yyyy}`;
        return today;
    },

    calcDate: function(date1, date2) {

        var date1 = date1;
        var date2 = new Date(date2 - date1);

        var message = date1.toString().substring(0,16) + " -- ";
        message += ((date2.toISOString().slice(0, 4) - 1970) + " years " + (date2.getMonth()+1) + " months " + date2.getDate() + " days ago.");

        return message;
    },

    calcMonth: function(monthText) {
        return months.indexOf(monthText.toLowerCase()) + 1;
      }

}
