const first = require('lodash.first')
const knex = require('../utils/database')

const removeMember = async (memberId) => {
    const users = await knex('users')
    .select('id')
    .where({'user_id': memberId})
    const user = first(users)
    if (user) {
        await knex('users')
        .where({
          'id': user.id
        })
        .del()

        await knex('userroles')
        .where({
          'userrole_user': user.id
        })
        .del()
      } else {
        return false
      }
}

module.exports = removeMember