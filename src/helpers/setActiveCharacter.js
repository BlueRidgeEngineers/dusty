const knex = require('../utils/database')

const activateCharacter = async (owner, character) => {
    await inactivateOtherCharactersInDatabase(owner, character)
    const activeCharacter = await saveActiveCharacterInDatabase(character)
    return activeCharacter
}

const saveActiveCharacterInDatabase = async (character) => {
    await knex
    .update({'char_activechar': 1})
    .from('web_characters')
    .where({'char_id': character})
    const dbChar = await knex.select('*')
        .from('web_characters')
        .where({'char_id': character})
        .pluck('char_designation')
    return dbChar.join("")
}

const inactivateOtherCharactersInDatabase = async (owner, character) => {
    await knex
    .update({'char_activechar': 0})
    .from('web_characters')
    .where({'char_user': owner})
    .whereNot({'char_id': character})
}

module.exports = activateCharacter