const level = require('./level')
const checkRole = require('./rolecheck')
const backup = require('./backupconfig')
const restore = require('./restoreconfig')
const generatePassword = require('./generatePassword')
const guildRoles = require('./guildroles')
const removeMember = require('./removeMember')
const botCommandToDB = require('./botCommandToDB')
const getDBMember = require('./getDBMember')
const activateCharacter = require('./setActiveCharacter')
const getCharacter = require('./getCharacter')
const getCharacterOwner = require('./getCharacterOwner')
const addRpXp = require('./addRpXp')
const datetime = require('./datetime')


module.exports = {
  level,
  checkRole,
  backup,
  restore,
  generatePassword,
  guildRoles,
  removeMember,
  botCommandToDB,
  getDBMember,
  activateCharacter,
  getCharacter,
  getCharacterOwner,
  addRpXp,
  datetime
}
