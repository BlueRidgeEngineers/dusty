const first = require('lodash.first')
const knex = require('../utils/database')

const getDBMember = async (author) => {
    const members = await knex.select('id')
        .from('users')
        .where({'user_id': author})
        .pluck('id')
    if(members) {
        return first(members)
    }
    else {
        return false
    }
}

module.exports = getDBMember
