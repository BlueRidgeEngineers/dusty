exports.up = (knex, Promise) => {
    return knex.schema.createTable('ch_role_lock', (table) => {
      table.increments('id')
      table.string('channel_id')
      table.string('role_id')
      table.integer('role_int')
    })
  }
  
  exports.down = (knex, Promise) => {
    return knex.schema.dropTable('ch_role_lock')
  }
