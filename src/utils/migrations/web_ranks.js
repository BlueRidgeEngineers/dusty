exports.up = (knex, Promise) => {
    return knex.schema.createTable('web_ranks', (table) => {
        table.increments('rank_id')
        table.integer('rank_tier')
        table.integer('rank_order')
        table.string('rank_name')
        table.string('rank_abbreviation')
        table.text('rank_description')
        table.integer('rank_xprequirement')
        table.integer('rank_approval')
        table.integer('rank_special')
        table.integer('rank_unique')

      })
    }
    
    exports.down = (knex, Promise) => {
      return knex.schema.dropTable('web_ranks')
    }