
exports.up = function(knex, Promise) {
    return knex.schema.raw(`ALTER TABLE public.users ADD COLUMN salt character varying(255), ADD COLUMN password character varying(255), ADD COLUMN tag character varying(255), ADD COLUMN nickname character varying(255), ADD COLUMN userset boolean`);
};

exports.down = function(knex, Promise) {
    return knex.schema.raw(`ALTER TABLE public.users DROP COLUMN salt, DROP COLUMN password, DROP COLUMN tag, DROP COLUMN nickname, DROP COLUMN userset`);
};


