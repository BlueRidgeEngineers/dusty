exports.up = (knex, Promise) => {
    return knex.schema.createTable('roles', (table) => {
      table.increments('role_id')
      table.string('role_snowflake')
      table.string('role_name')
      table.string('role_guild')
    })
  }
  
  exports.down = (knex, Promise) => {
    return knex.schema.dropTable('roles')
  }