exports.up = (knex, Promise) => {
    return knex.schema.createTable('bot_commands', (table) => {
      table.increments('command_id')
      table.string('command_author')
      table.string('command_message')
    })
  }
  
  exports.down = (knex, Promise) => {
    return knex.schema.dropTable('bot_commands')
  }