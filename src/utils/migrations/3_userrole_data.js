exports.up = (knex, Promise) => {
    return knex.schema.createTable('userroles', (table) => {
      table.increments('userrole_id')
      table.integer('userrole_user')
      table.integer('userrole_role')
    })
  }
  
  exports.down = (knex, Promise) => {
    return knex.schema.dropTable('userroles')
  }