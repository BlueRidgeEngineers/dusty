const express = require('express')
const cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser')
const path = require('path');
var morgan = require('morgan')
const app = express()
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(morgan(':remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms'))
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

var pg = require('pg')
  , session = require('express-session')
  , pgSession = require('connect-pg-simple')(session);
 
var pgPool = new pg.Pool({
  database: process.env.POSTGRES_DATABASE,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  table: 'session',
  port: 5432,
  ssl: false,
  max: 20, // set pool max size to 20
  min: 4, // set min pool size to 4
  idleTimeoutMillis: 1000, // close idle clients after 1 second
  connectionTimeoutMillis: 1000,
})
 
app.use(session({
  store: new pgSession({
    pool : pgPool
  }),
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 } // 30 days
}));

module.exports = app