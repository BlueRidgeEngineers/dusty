const discord = require('discord.js')
const bot = new discord.Client()
require('dotenv').config()
bot.login(process.env.DISCORD_TOKEN)

const { prefix } = require('./constants')
// additional botfunctions in rolechecker.js, probably rename at a later time
const { guildRoles, removeMember, level, botCommandToDB, getDBMember, addRpXp } = require('./helpers')
const commands = require('./commands')

//this was added
bot.on('ready', () => {
  bot.user.setActivity('Type: $help')
})

// bot methods
bot.on('ready', async () => {
  await bot.user.setStatus(`online`, `Say ${prefix}help`)
  console.log(`DUST-3 Online`)
  const guilds = await bot.guilds.array()
  console.log(`Joined servers: ${guilds.length}`)
  guilds.forEach(async function (guild) {
    console.log(`Joined server ${guild.name} with ID ${guild.id} \n Owner: ${guild.owner.user.tag} \n username: ${guild.owner.user.username} \n id: ${guild.owner.user.id} \n`)
    if(guild.id!=process.env.GUILD_ID) {
      guild.leave()
      .then(g => console.log(`Left the guild ${g}`))
      .catch(console.error);
    }
  })

  bot.on('guildMemberUpdate', async (oldMember, newMember) => {
    const memberInfo = {
      'guild': newMember.guild.id,
      'tag': newMember.user.tag,
      'nickname': newMember.nickname,
      'roles': newMember.roles,
      'userid': newMember.id
    }
    const oldMemberInfo = {
      'guild': oldMember.guild.id,
      'tag': oldMember.user.tag,
      'nickname': oldMember.nickname,
      'roles': oldMember.roles,
      'userid': oldMember.id
    }
    await level.updateUser(memberInfo, oldMemberInfo)
  })

  bot.on('guildMemberRemove', async member => {
    removeMember(member.id) 
  })

  const promises = guilds.map(async guild => {
    const members = guild.members.array()
    const roles = guild.roles.array()
    roles.map(async role => {
      const roleInfo = {
        'snowflake': role.id,
        'guild': role.guild.id,
        'name': role.name
      }
      guildRoles.addRole(roleInfo)
    })
    members.map(async member => {
      const memberInfo = {
        'guild': guild.id,
        'tag': member.user.tag,
        'nickname': member.nickname,
        'roles': member.roles,
        'userid': member.id
      }
      level.addUser(memberInfo)
    })
    
  })
  await Promise.all(promises)
})

bot.on('error', async error => {
  console.log(`charord.js error: ${error}`)
})

bot.on('roleCreate', async role => {
  console.log(`${role.name} created on ${role.guild.name}.`)
  const roleInfo = {
    'snowflake': role.id,
    'guild': role.guild.id,
    'name': role.name
  }
  await guildRoles.addRole(roleInfo)
})

bot.on('roleUpdate', async (oldRole, newRole) => {
  console.log(`${oldRole.name} updated on ${oldRole.guild.name}.`)
  const roleInfo = {
    'snowflake': newRole.id,
    'guild': newRole.guild.id,
    'name': newRole.name
  }
  await guildRoles.updateRole(roleInfo)
})

bot.on('roleDelete', async role => {
  console.log(`${role.name} deleted on ${role.guild.name}.`)
  const roleInfo = {
    'snowflake': role.id,
    'guild': role.guild.id,
    'name': role.name
  }
  await guildRoles.deleteRole(roleInfo)
})

bot.on('messageReactionAdd', async (messageReaction, user) => {
  if((await botCommandToDB.getBotCommandAuthor(messageReaction.message.id) == await getDBMember(user.id)) || (await messageReaction.message.channel.guild.members.find('id', user.id).roles.has(await messageReaction.message.channel.guild.roles.find('name', "Staff").id)) && messageReaction.me) {
    await botCommandToDB.deleteBotCommandFromDB(messageReaction.message.id)
    await messageReaction.message.delete(500)

  } 
  
})

bot.on('message', async msg => {
  // if not something the bot cares about, exit out
  if (msg.author.bot || msg.system || msg.tts || msg.channel.type === 'dm') return

  // once every x minutes, give poster y xp
  if (!msg.content.startsWith(prefix)) { 
    level.msgXp(msg, 3, 3)
    addRpXp(msg.content, msg.channel.id, msg.author.id)
    return
  }
  // Trim the mention from the message and any whitespace
  let command = msg.content.substring(msg.content.indexOf(prefix), msg.content.length).trim().toLowerCase()
  if (command.startsWith(prefix)) {
    const queryArr = command.split(prefix).slice(1).join().split(' ')
    let commandToExecute = queryArr.shift()
    let argument = queryArr.join(' ')
    if (commands[commandToExecute]) {
      if(!(await msg.guild.members.find('id', msg.author.id).roles.has(await msg.guild.roles.find('name', "Blacklisted").id))) {
        try {
          await commands[commandToExecute].process(msg, argument)
        } catch (error) {
          console.log(error)
        }
      }
      else {
        msg.react('😡')
        .then(async function() {
          setTimeout(function() {msg.delete()},1000)
        })
      }
    } else {
      msg.react('🤷')
    }
  }
})

bot.on('guildMemberAdd', async member => {
  const guild = member.guild
  console.log(`${member.user.username} joined ${guild.name}.`)
  const memberInfo = {
    'guild': guild.id,
    'tag': member.user.tag,
    'nickname': member.nickname,
    'roles': member.roles,
    'userid': member.id
  }
  const ourMission = bot.channels.find('name', "our-mission-and-rules")
  const greeters = guild.roles.find('name', "Greeter")
  bot.channels.get(process.env.WELCOME_CHANNEL).send(`Welcome, ${member} to the Blue Ridge Brotherhood! Check out ${ourMission} to get an idea what we're about! Talk to one of our ${greeters} to help get you settled in, and answer any questions you might have.`)
  await level.addUser(memberInfo)
})

bot.on("guildCreate", guild => {
  if(guild.id!=process.env.GUILD_ID) {
    guild.leave()
    .then(g => console.log(`Left the guild ${g} as it's not ours!`))
    .catch(console.error);
  }
});

module.exports = bot
