const commands = {
  'lint': require('./generic/lint'), //privileged only
  'xp': require('./generic/xp'), //privileged only
  'revoke': require('./generic/revoke'), //privileged, not implemented atm
  'reboot': require('./generic/reboot'), //privileged only
  'enlist': require('./generic/enlist'), // privileged only
  'backup': require('./generic/backup'), //privs required
  'restore': require('./generic/restore'), //privs required
  'seedusers': require('./generic/seedusers'), //privs required
  'silent': require('./generic/lockch'), //privs required
  'speak': require('./generic/unlockch'), //privs required
  'clear': require('./generic/clear'), //privs required
  'help': require('./help'), //for all users \/
  'rp': require('./generic/rp'),
  'xplu': require('./generic/xplu'),
  'whoishere': require('./generic/whoishere'),
  'propaganda': require('./generic/propaganda'),
  'password': require('./generic/password'),
  'info': require('./generic/info'),
  'level': require('./generic/level'),
  'userid': require('./generic/userid'),
  'map': require('./generic/map'), 
  'ping': require('./generic/ping'),
  'time': require('./generic/time'),
  'info': require('./generic/info'),
  'roll': require('./roleplay/roll'),
  'characters': require('./roleplay/characters'),
  'charsheet': require('./roleplay/charsheet'),
  'playcharacter': require('./roleplay/playcharacter'),
  'activecharacter': require('./roleplay/activecharacter'),
  //'id': require('./generic/user_ident'),
  'pc': require('./generic/platf_selector'),
  'xbox': require('./generic/platf_selector'),
  'ps4': require('./generic/platf_selector')
}

module.exports = commands