const botCommandToDB = require('../../helpers/botCommandToDB')
const knex = require('../../utils/database')

module.exports = {
  description: 'Shows your currently active character.',
  usage: '',
  process: async (msg, argument) => {
    let output = ""
    const character = await knex.first('char_designation', 'char_name', 'char_nickname', 'char_lastname')
    .from('web_characters')
    .innerJoin('users', {'users.id': 'web_characters.char_user'})
    .where({'users.user_id': msg.author.id, 'web_characters.char_activechar': 1})
    if(character) {
      output = output + (`${msg.author} you are currently playing `)
      output = output + (`** ${character.char_designation} - ${character.char_name} `)
      if(character.char_nickname != null) {
          output = output + `"${character.char_nickname}" `
      }
      output = output + `${character.char_lastname} **`
      msg.channel.send(`${output}`)
      .then(async function (newmsg) {
        await msg.delete()
        setTimeout(function() {newmsg.delete()},5000)
      })
      .catch(function() {
        //Something
       });
    }
    else {
      output = output + (`${msg.author} you currently have no active character!`)
      msg.channel.send(`${output}`)
      .then(async function (newmsg) {
        await msg.delete()
        setTimeout(function() {newmsg.delete()},5000)
      })
      .catch(function() {
        //Something
       });
    }
  }
}
