const knex = require('../../utils/database')
const botCommandToDB = require('../../helpers/botCommandToDB')

module.exports = {
  description: 'Roll some dice. Possible values N: 1,2,3,4,5 M: 4,6,8,10,12,20,100, optional modifiers: positive(+) or negative(-) numbers, characters S,P,E,C,I,A,L',
  usage: '<N>d<M>[Modifier] ==> $roll 2d10, $roll 4d20+5, $roll 5d100-10, $roll 1d20S',
  process: async (msg, argument) => {
    const dbStats = await knex.first('char_s', 'char_p', 'char_e', 'char_c', 'char_i', 'char_a', 'char_l', )
    .from('web_characters')
    .innerJoin('users', {'users.id': 'web_characters.char_user'})
    .where({'users.user_id': msg.author.id, 'web_characters.char_activechar': 1})
    var statObj = {
      'S': 0,
      'P': 0,
      'E': 0,
      'C': 0,
      'I': 0,
      'A': 0,
      'L': 0
    }
    if(dbStats) {
      statObj = {
        'S': dbStats.char_s,
        'P': dbStats.char_p,
        'E': dbStats.char_e,
        'C': dbStats.char_c,
        'I': dbStats.char_i,
        'A': dbStats.char_a,
        'L': dbStats.char_l
      }
    }
    var strippedArgument = argument.replace(/\s+/g, '');
    var availableDice = [1,2,3,4,5]
    var availableDigits = [4,6,8,10,12,20,100]
    let numbers = strippedArgument.split('d');
    var dice = parseInt(numbers[0],10)
    var digits = parseInt(numbers[1],10)
    var modifier = 0
    var matchModifier = /[+-]+?/.exec(numbers[1],10)
    var matchSpecial = /[SPECIAL]+?/.exec(numbers[1],10)

    if(matchModifier) {
      digits = parseInt((numbers[1]).substring(0, (matchModifier.index)),10)
      modifier = parseInt((numbers[1]).substring((matchModifier.index), (numbers[1],10).length),10) 
    }
    if(matchSpecial) {
      digits = parseInt((numbers[1]).substring(0, (matchSpecial.index)),10)
      modifier = statObj[matchSpecial[0]]
      strippedArgument=strippedArgument+` **${strippedArgument.substring(strippedArgument.length-1, strippedArgument.length)}=${modifier}**`
    }

    if(numbers.length != 2 || !availableDice.includes(dice) || !availableDigits.includes(digits)) {
      msg.channel.send(`Try again with valid input ==> $roll 5d20 or see $rp`)
      .then(async function (newmsg) {
        await msg.delete()
        setTimeout(function() {newmsg.delete()},5000)
      })
    }
    else {
      let results = []
      let sum
      for(var x=0;x<dice;x++) {
        results.push(Math.floor((Math.random() * digits) + 1) +modifier)
      }
      sum = results.reduce(function(pv, cv) { return pv + cv; }, 0);
      msg.channel.send(`${msg.author} rolled some dice (${strippedArgument}): ${results.join(" + ")} = **${sum}**`)
      .then(async function (newmsg) {
        await newmsg.react("❌");
        await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
        await msg.delete() 
      }) 
      console.log(`${msg.author.username} rolled some dice (${strippedArgument}): ${results.join(" + ")} = **${sum}**`)     
    }
    //await console.log(`${msg.channel.send} Test: `)
  
  }
}
