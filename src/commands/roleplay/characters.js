const botCommandToDB = require('../../helpers/botCommandToDB')
const knex = require('../../utils/database')
const first = require('lodash.first')

module.exports = {
  description: 'Lists all the characters of the message author and their codes.',
  usage: '',
  process: async (msg, argument) => {
    let output = ""
    var member = await msg.guild.members.find('id', msg.author.id)
    const characters = await knex.select('char_designation', 'char_name', 'char_nickname', 'char_lastname')
    .from('web_characters')
    .innerJoin('users', {'users.id': 'web_characters.char_user'})
    .where({'users.user_id': msg.author.id})
    if(first(characters)) {
      output = output + (`${msg.author}'s characters: \n`)
      output = output + (`\`\`\``)
      let count = 1
      characters.forEach(async function (character) {
        output = output + (`${count}: ${character.char_designation} - ${character.char_name} `)
        if(character.char_nickname != null) {
          output = output + `"${character.char_nickname}" `
        }
        output = output + `${character.char_lastname}\n`
        count++
      })
      output = output + (`\`\`\``)
      msg.channel.send(`${output}`)
      .then(async function (newmsg) {
        await newmsg.react("❌");
        await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
        await msg.delete() 
      })
      .catch(function() {
        //Something
       });
    }
    else {
      output = output + (`${msg.author} you currently have no characters!`)
      msg.channel.send(`${output}`)
      .then(async function (newmsg) {
        await msg.delete()
        setTimeout(function() {newmsg.delete()},5000)
      })
      .catch(function() {
        //Something
       });
    }
  }
}
