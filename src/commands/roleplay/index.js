const commands = {
  'roll':require('./roll'),
  'characters':require('./characters'),
  'charsheet': require('./charsheet'),
  'playcharacter': require('./playcharacter'),
  'activecharacter': require('./activecharacter')
  
  }
  
  module.exports = commands
  