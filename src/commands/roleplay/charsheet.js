const botCommandToDB = require('../../helpers/botCommandToDB')
const Discord = require('discord.js')
const first = require('lodash.first')
const knex = require('../../utils/database')

module.exports = {
  description: 'Shows a specific character sheet',
  usage: '<designation> ==> $charsheet AB-123I, omit designation for active characters sheet',
  process: async (msg, argument) => {
    if(argument != "") {
      var characters = await knex.select('*')
        .from('web_characters')
        .innerJoin('web_ranks', {'web_characters.char_rank': 'web_ranks.rank_id'})
        .where({'char_designation': argument})
        
    }
    else {
      var characters = await knex.select('*')
        .from('web_characters')
        .innerJoin('users', {'users.id': 'web_characters.char_user'})
        .innerJoin('web_ranks', {'web_characters.char_rank': 'web_ranks.rank_id'})
        .where({'users.user_id': msg.author.id, 'web_characters.char_activechar': 1})
        
    }
    const statuses = {
      1: "Alive",
      2: "Dead",
      3: "Missing"
    }
  character = first(characters)
    console.log(`${msg.author} used charsheet: ${character.char_nickname}`)
    if(character) {
      var name = []
      name.push(`${character.char_name} `)
      if(character.char_nickname != null) {
        name.push(`"${character.char_nickname}" `)
      }
      name.push(`${character.char_lastname}`)
      name.join(" ")
      const embed = new Discord.RichEmbed()
      .setTitle(`${character.char_designation}: ${name.join("")}`)
      .setURL(`https://www.blueridgebos.com/guild/members/character/view/${character.char_id}`) 
      .setThumbnail(`https://www.blueridgebos.com/static/images/characters/pictures/${character.char_portrait}`)
      .setImage(`https://www.blueridgebos.com/static/images/characters/pictures/${character.char_picture}`)
      .addField("Background", (`${((character.char_description).substring(0, 140))}...`), false)     
      .addField("Physiological Examination", (`${((character.char_physical).substring(0, 140))}...`), false)     
      .addField("Psychological Examination", (`${((character.char_psyche).substring(0, 140))}...`), false)  
      .addField("Rank", (character.rank_name), true)    
      .addField("Age", (character.char_age), true)  
      .addField("Sex", (character.char_sex), true)  
      .addField("Height", (character.char_height), true)      
      .addField("Weight", (character.char_weight), true)     
      .addField("Eye Color", (character.char_eyecolor), true)     
      .addField("Hair Color", (character.char_haircolor), true)  
      .addField("Radiation", (character.char_radiation), true)     
      .addField("Status", (statuses[character.char_status]), true)  
      .addField("Stats", (`**S** ${character.char_s} **P** ${character.char_p} **E** ${character.char_e} **C** ${character.char_c} **I** ${character.char_i} **A** ${character.char_a} **L** ${character.char_l}`), true)      
      
      .setFooter("Powered by the Brotherhood Scribes", msg.guild.iconURL)
      .setColor('DARK_BLUE')   
      await msg.channel.send( { embed } )
      .then(async function (newmsg) {
        await newmsg.react("❌")
        await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id)
        await msg.delete() 
      })
      .catch(function() {
        //Something
      });
    }
    else {
      var output = ""
      output = output + (`${msg.author} that character doesn't exist!`)
      msg.channel.send(`${output}`)
      .then(async function (newmsg) {
        await msg.delete()
        setTimeout(function() {newmsg.delete()},5000)
      })
      .catch(function() {
        //Something
      });
    }
  }
    
}
