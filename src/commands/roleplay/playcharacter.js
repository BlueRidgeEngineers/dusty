const Discord = require('discord.js')
const knex = require('../../utils/database')
const first = require('lodash.first')
const { getCharacter, getCharacterOwner, activateCharacter, getDBMember, botCommandToDB } = require('../../helpers')

module.exports = {
  description: 'Sets the defined character as your active character. You will get discord-roleplay XP for this character.',
  usage: '<designation> ==> $playcharacter AB-123I',
  process: async (msg, argument) => {
    let character = await getCharacter(argument)
    character = parseInt(character)
    if(!Number.isNaN(character)) {
      if(await getCharacterOwner(character) == await getDBMember(msg.author.id)) {
        const activeCharacter = await activateCharacter(await getCharacterOwner(character), character)
        msg.channel.send(`**Beep beep boop!** ${msg.author}! Character **${activeCharacter}** activated!`)
        .then(async function (newmsg) {
          await msg.delete()
          setTimeout(function() {newmsg.delete()},5000)
        })
      }
    }
    else {      
      msg.channel.send(`**Beep beep boop!** ${msg.author}! Something went wrong!`)
      .then(async function (newmsg) {
        await msg.delete()
        setTimeout(function() {newmsg.delete()},5000)
      })
    }
    /*if() {
    }
    else {
      
    }*/
  }
}
