const {  checkRole } = require('../../helpers')

module.exports = {
  description: 'Enlist a new member to the faction. Requires permissions.',
  process: async msg => {
    if (await checkRole(msg, ['Head Scribe', 'Admin', 'Elder', 'Moderator', 'Moderator In Training', 'Greeter'])) {
      if (msg.mentions.users.first() !== undefined) {
        let target = msg.guild.member(msg.mentions.users.first())
        await target.addRoles([msg.guild.roles.find('name', 'Initiate').id])
        await target.setNickname(`Initiate ${target.user.username}`)
        return target.user.send('Welcome, ' + target.nickname)
      } else {
        await msg.channel.send('Mention a user to enlist.')
        .then(async function (newmsg) {
          await newmsg.react("❌");
          await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
          await msg.delete() 
        })
      }
    } else {
      await console.log(`${msg.author.username} tried to use a command without privs`)
    }
    
    // return bot.reject(msg)
  }
}
