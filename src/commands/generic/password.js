const { checkRole, generatePassword, botCommandToDB } = require('../../helpers')

module.exports = {
  description: 'Get or reset your password for the website',
  process: async (msg) => {
    if (await checkRole(msg, ['Elder', 'Engineers', 'Game Master', 'Lorekeeper', 'Brotherhood Member'])) {
      await generatePassword(msg)
      await msg.delete()
    } else {
        await msg.channel.send('You have to be a member of our faction for that!')
        .then(async function (newmsg) {
          await newmsg.react("❌");
          await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
          await msg.delete() 
        })
      }
  }
}
