const path = require('path')
const {botCommandToDB} = require('../../helpers')

module.exports = {
  description: 'Display the latest map we have for FO76.',
  process: async msg => {
    const mapImagePath = path.join(__dirname, '../../../public/images/Fallout76Map.png')
    await msg.channel.send({ files: [mapImagePath] })
    .then(async function (newmsg) {
      await newmsg.react("❌");
      await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
      await msg.delete() 
    })
  }
}
