const Discord = require('discord.js')

module.exports = {
	description: '$time ',
	usage: `Usage :original timezone (space) Hour(format HH:MM) (space) pretended Time zone
	Example:HST 09:50 PST
	specific help command $time help`,
	process: async msg => {

		
	//Comand created by difficulty#3998 for Dust-3 bot from Blue Ridge B.o.S
	
	//
	var timez=['MIT','HST','AST','PST','PNT','MST','CST','EST','IET','PRT','AGT','BET','CAT','UTC','ECT','EET','ART','MSK','EAT','NET','PLT','BST','VST','CTT','JST','AET','SST','NST'];
	var timeztoutc=[-11,-10,-9  ,-8   ,-7   ,-7   ,-6   ,-5   ,-5   ,-4   ,-3   ,-3   ,-1   ,0    ,1    ,2    ,2    ,3    ,3    ,4    ,5    ,6    ,7    ,8    ,9    ,10   ,11   ,12   ];
	var time = msg.content.split(" ");
	time.shift();
	await console.log(`${msg.author.username} used command '$time'`)

	if((time.length)==1 && time[0].toString().toLowerCase().valueOf() == new String("help").valueOf() )
	{
	var embed = new Discord.RichEmbed()
	.setTitle(`Timezone Help`)
	.addField("Usage", "$time <your timezone> <your time> <convert time>",false)
	.addField("Timezones", "GMT	- Greenwich Mean Time\nUTC	- Universal Coordinated Time\nECT - European Central Time\nEET - Eastern European Time\nART - (Arabic) Egypt Standard Time\nEAT - Eastern African Time\nMET - Middle East Time\nNET - Near East Time\nPLT - Pakistan Lahore Time\nIST - India Standard Time\nBST - Bangladesh Standard Time\nVST - Vietnam Standard Time\nCTT - China Taiwan Time\nJST - Japan Standard Time\nACT - Australia Central Time\nAET - Australia Eastern Time\nSST - Solomon Standard Time\nNST - New Zealand Standard Time\nMIT - Midway Islands Time\nHST - Hawaii Standard Time\nAST - Alaska Standard Time\nPST - Pacific Standard Time\nPNT - Phoenix Standard Time\nMST - Mountain Standard Time\nCST - Central Standard Time\nEST - Eastern Standard Time\nIET - Indiana Eastern Standard Time\nPRT - Puerto Rico and US Virgin Islands Time\nCNT - Canada Newfoundland Time\nAGT - Argentina Standard Time\nBET - Brazil Eastern Time\nCAT - Central African Time")
	.addField("Description", "**Example:** $time HST 09:50 EET\nIf your time zone doesn't exists in the bot or you have any other problem(s) with timezones please contact the Scribes.")
	.setFooter("Powered by the Brotherhood Scribes", msg.guild.iconURL)
	.setColor('DARK_BLUE')  
	await msg.channel.send( { embed } )
	.catch(function() {
	  //Something
	})

	//await msg.channel.send("This command is used in the following way: original timezone (space) Hour(format HH:MM) (space) pretended Time zone\nExample:HST 09:50 EET\nAvailable time zones:"+timez.join(",")+"\n source of time zones:https://publib.boulder.ibm.com/tividd/td/TWS/SC32-1274-02/en_US/HTML/SRF_mst273.htm\nIf your time zone dosen't exist in the bot or any other problem whit timezones please contact difficulty#3998 or the head Scribe")
	}
	else{

		if((time.length)!=3 && time.length!=2)
		await msg.channel.send('error please use the comand "$time help" to know more')
		else{
			var msg1="";
			var utcH=0;
			var utcM=0;
			var aux=time[1].split(":");
			var hour1=aux[0];
			var minute1=aux[1];
			var hour2=0;
			var minute2=0;
			var x;
			var count=0;
			var msgF="";
			for(x=0;x<timez.length;x++)
			if(time[0].valueOf() == timez[x].valueOf())
			{
				utcH=parseInt(hour1)-timeztoutc[x];
				utcM=parseInt(minute1);
				break;
			}
			else{
				count++;
			}
			if(count==timez.length)
			{await msg.channel.send('error please use the comand "$time help" to know more')}
			else{
			if(utcM>60)
			{
				utcM-=60;
				utcH++;
			}
			else if(utcM<0)
			{
				utcM+=60;
				utcH--;
			}
			if(utcH<0)
				utcH+=24;
			else if(utcH>24)
					utcH-=24;

			count=0;
			//msg1="@everyone Original time: "+time[1];
			msg1="Original time: "+time[1];
			if(time.length==2)
			{
				for(x=0;x<timez.length;x++){
				hour2=parseInt(utcH)+timeztoutc[x];
				minute2=parseInt(utcM);


				if(minute2>60)
				{
					minute2-=60;
					hour2++;
				}
				if(minute2<0)
				{
					minute2+=60;
					hour2--;
				}
				if(hour2>24){
					hour2-=24;
					msgF="+1 day";
			}
				if(hour2<0){
					hour2+=24;
					msgF="-1 day";
			}

 
				if(hour2==0)
				msg1+="\nnew Time: 0"+hour2;
				else
				msg1+="\nnew Time: "+hour2;
				if(minute2==0)
				msg1+=":0"+minute2;
				else
				msg1+=":"+minute2;

				msg1 +=" "+timez[x];
				msg1 +=msgF;

				msgF="";
				}
			}
			else{
				
				for(x=0;x<timez.length;x++){
					if(time[2].valueOf() == timez[x].valueOf())
					{
						hour2=parseInt(utcH)+timeztoutc[x];
						minute2=parseInt(utcM);
						break;
					}
					else count++
				}
				if(count==timez.length)
				{await msg.channel.send('error please use the comand "$time help" to know more')}
				else{
					if(minute2>60)
					{
						minute2-=60;
						hour2++;
					}
					if(minute2<0)
					{
						minute2+=60;
						hour2--;
					}
					if(hour2>24){
							hour2-=24;
							msgF="+1 day";
					}
					if(hour2<0){
						hour2+=24;
						msgF="-1 day";
					}
	
					if(hour2==0)
					msg1+="\nnew Time: 0"+hour2;
					else
					msg1+="\nnew Time: "+hour2;
					if(minute2==0)
					msg1+=":0"+minute2;
					else
					msg1+=":"+minute2;
	
					msg1 +=" "+timez[x];
					msg1 +=msgF;

					msgF="";
				}
					
			}
			await msg.channel.send(msg1+'')
			}
		}
	}
}
}
