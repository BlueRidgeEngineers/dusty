const {  checkRole, botCommandToDB } = require('../../helpers')

module.exports = {
  description: '',
  process: async msg => {

    let platform = msg.content.substring(1,3).toUpperCase()
      switch (platform) {
        case "PC":
          platform = "PC";
          break;
        case "PS":
          platform = "PS4";
          break;
        case "XB":
          platform = "Xbois";
          break;
      
        default:
          return;
          break;
      }
    

            //-----------Mention role adding-----------
      if (msg.mentions.users.first() !== undefined) {
    if (await checkRole(msg, ['Head Scribe', 'Admin', 'Elder', 'Moderator', 'Moderator In Training', 'Showrunner','Greeter'])) {
          let target = msg.mentions.members.first();
        if (!target.roles.has(msg.guild.roles.find('name', platform).id)) {
          await target.addRoles([msg.guild.roles.find('name', platform).id])
          const platform_channel =  msg.guild.channels.find(channel => channel.name === platform.toLowerCase()).toString();
          msg.channel.send(`Welcome ${target} in the ${platform} Group. Head to your teammates in ${platform_channel}`)
          return msg.delete();
        }
        else {
          await msg.channel.send(`${target} already has '${platform}' role.`)
            .then(async function (newmsg) {
            await newmsg.react("❌");
            await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
            await msg.delete() 
            }) 
        }
      }
    else {
      await console.log(`${msg.author.username} tried to use a command without privs`)
    }
      }

      //-----------self role adding-----------
      if (msg.mentions.users.first() == undefined) {
        if (!msg.member.roles.has(msg.guild.roles.find('name', platform).id)) {
          const target = msg.guild.member(msg.author);
          await target.addRoles([msg.guild.roles.find('name', platform).id])
          const platform_channel =  msg.guild.channels.find(channel => channel.name === platform.toLowerCase()).toString();
          msg.channel.send(`Welcome to the ${platform} Group. Head to your teammates in ${platform_channel}`)
          return msg.delete();
        }
        else {
          await msg.channel.send(`${msg.author} you already have '${platform}' role.`)
            .then(async function (newmsg) {
            await newmsg.react("❌");
            await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
            await msg.delete() 
            }) 
        }
      }
    
    // return bot.reject(msg)
  }
}