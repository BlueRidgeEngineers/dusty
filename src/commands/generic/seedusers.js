const {  checkRole } = require('../../helpers')
const knex = require('../../utils/database')

module.exports = {
  description: 'Saves some user info to the db, only required during development, fields will also be added automatically once new user joins server',
  discrete: true,
  process: async msg => {
    if (await checkRole(msg, ['Head Scribe', 'Admin', 'Elder', 'Webmaster'])) {
      msg.guild.members.forEach(async function (member) {
        await knex('users')
        .where({
            'user_id': member.id,
            'server_id': msg.guild.id
        })
        .update({
            'tag': member.user.tag,
            'nickname': member.nickname
        })
      })
      
      await console.log(`${msg.author.username} seeded usertags to DB`)
    } else {
      await console.log(`${msg.author.username} tried to use a command without privs`)
    }
    
    // return bot.reject(msg)
  }
}
