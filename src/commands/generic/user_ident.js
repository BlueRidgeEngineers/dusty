const { checkRole, botCommandToDB, datetime } = require('../../helpers')
const Discord = require('discord.js')

module.exports = {
    description: "User Identification",
    usage: "$id <user_mention>",
    discrete: true,
    process: async msg => {

            if (msg.mentions.users.first() !== undefined) {
        if (await checkRole(msg, ['Elder', 'Admin', 'Moderator', 'Head Scribe', 'Moderator In Training'])) { 
            let target = msg.mentions.members.first();
            userID(target, msg);
        }
        }
        
        if (msg.mentions.users.first() == undefined) {
            userID(msg.author, msg);
        }
    }
}

async function userID (target, msg) {

    var target = target;
    var avatarURL;
    try { avatarURL = target.user.avatarURL; } catch(err) { avatarURL = target.avatarURL; }
    var joinedDiscord = msg.guild.member(target).user.createdAt.toString().substring(0,16)
    var JoinedDDay = joinedDiscord.substring(8,11)
    var JoinedDMonth = datetime.calcMonth(joinedDiscord.substring(4,8));
    var JoinedDYear = joinedDiscord.substring(11,15)

    var joinedServer = msg.guild.member(target).joinedAt.toString().substring(0,16)
    var JoinedSDay = joinedServer.substring(8,11)
    var JoinedSMonth = datetime.calcMonth(joinedServer.substring(4,7));
    var JoinedSYear = joinedServer.substring(11,15)

    const embed = new Discord.RichEmbed()   
        .setTitle("User Identification")
        .setDescription(`${target.presence['status']} - ${target}`)
        .setThumbnail(avatarURL)
        .addField("User ID", `${target.id}`, false)     
        .addField("Joined Discord", `${datetime.calcDate(new Date(JoinedDYear,JoinedDMonth,JoinedDDay), new Date(datetime.now()))}`, false)
        .addField("Joined Server", `${datetime.calcDate(new Date(JoinedSYear,JoinedSMonth,JoinedSDay), new Date(datetime.now()))}`, false)
        .addField("Highest Role", `${msg.guild.member(target).highestRole}`, false)
        
        .setFooter("Powered by the Brotherhood Scribes", msg.guild.iconURL)
        .setColor('DARK_BLUE')

    await msg.channel.send( { embed } )
    .then(async function (newmsg) {
        await newmsg.react("❌")
        await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id)
        await msg.delete() 
    })
    .catch(function(err) {
        console.log(`Error at user_ident.js - `+ err)
    });
}