const { checkRole } = require('../../helpers')
const path = require('path')
const knex = require('../../utils/database')
const first = require('lodash.first')

module.exports = {
    description: 'Lock the Text channel, so regular people can not write or add reaction.',
    usage: '$silence',
    discrete: true,
    process: async msg => {
        if(await checkRole(msg, ['Elder', 'Admin', 'Moderator', 'Head Scribe', 'Moderator In Training'])){
            const imagePath = path.join(__dirname, '../../../public/images/silence_bos.png')
            await msg.channel.send({file: imagePath})

            let channel_id = msg.channel.id

            msg.channel.permissionOverwrites.forEach(async function (perm) {

              const channels = await knex
                .select('*')
                .from('ch_role_lock')
                .where({
                  'channel_id': channel_id})

              const dbChannel = first(channels)
              if (!dbChannel && hasRole(perm.id)) { 
                console.log('Adding channel roles to DB...')
                await knex('ch_role_lock').insert({
                    'channel_id': channel_id,
                    'role_id': perm.id,
                    'role_int': perm.allow
                }).catch((err) => {console.log(err);});

                msg.channel.overwritePermissions(perm.id, {SEND_MESSAGES: false, ADD_REACTIONS: false})
            }
            else {
                console.log(`'${perm.channel.name}' already in lockdown with role(s) ${perm.id} OR Skipped.`)
            }

            })

            console.log(`${msg.channel.name} Locked.`)
        }
    }
}

function hasRole(id) {
    if (id == 458390674048548875 || //elder
        id == 458384244058619927 || //admin
        id == 458787744873971712 || //moderator
        id == 458699856513335306 || //MiT
        id == 458405164156452875) { //Head Scribe
        return false;
    }
    else {
        return true;
    }
}

/*
Elder id: 458390674048548875
Admin id: 458384244058619927
Moderator id: 458787744873971712
MiT id : 458699856513335306
Head Scribe id: 458405164156452875
Risky's role id: 459595533556776960 
*/
