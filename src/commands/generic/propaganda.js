const fs = require('fs')
const sample = require('lodash.sample')
const path = require('path')
const {botCommandToDB} = require('../../helpers')

module.exports = {
  description: 'Display a piece of BoS propaganda.',
  process: async msg => {
    const imagesPath = path.join(__dirname, '../../../public/images/propaganda')
    fs.readdir(imagesPath, (err, files) => {
      if (err) {
        return msg.channel.send('No assets found.')
        .then(async function (newmsg) {
          await newmsg.react("❌");
          await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
          await msg.delete() 
        })
      
      }
      let validFiles = files.filter(file => !file.startsWith('.'))
      let filepath = path.join(imagesPath, sample(validFiles))
      return msg.channel.send({ files: [filepath] })
      .then(async function (newmsg) {
        await newmsg.react("❌");
        await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
        await msg.delete() 
      })
    })
  }
}
