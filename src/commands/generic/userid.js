const botCommandToDB = require('../../helpers/botCommandToDB')

module.exports = {
  description: 'Check if the bot is online.',
  discrete: true,
  process: async (msg, argument) => {
    const memberName = await msg.guild.members.find('id', argument).nickname
    msg.channel.send(memberName)
    .then(async function (newmsg) {
      await newmsg.react("❌");
      await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
      await msg.delete() 
    }) 
    await console.log(`${msg.author.username} pinged the bot`)
  }
}
