const { prefix } = require('../../constants')

module.exports = {
  description: 'An overview of all roleplay commands',
  process: async msg => {
    const rolePlayCommands = require('./../roleplay')
    let commandList = 'Available roleplay commands:```'
    for (const cmd in rolePlayCommands) {
      if (!rolePlayCommands[cmd].discrete) {
        let command = prefix + cmd
        let usage = rolePlayCommands[cmd].usage
        if (usage) {
          command += ' ' + usage
        }
        let description = rolePlayCommands[cmd].description
        if (description) {
          command += '\n\t' + description
        }
        commandList += command + '\n\n'
      }
    }
    commandList += '```'
    msg.author.send(commandList)
  }
}