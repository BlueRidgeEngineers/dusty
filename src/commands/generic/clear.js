const { level, checkRole } = require('../../helpers')

module.exports = {
  description: '$clear <1-99> - Delete Last x mensages.',
  process: async message => {

    if (await checkRole(message, ['Head Scribe', 'Admin', 'Elder','Moderator'])) {
      //removes the spcified nunber of mensages
      var time = message.content.split(" ");
      var times=0;
      if(time.length>1){
        times=time[1];
      }
      else {
        times=1;
      }
      //console.log(times+"");
      var a=[];
      times++;
      if(times<=100 && times>1){
        await message.channel.fetchMessages({ limit: times })
        .then(messages => {for(var [key, value] of messages) { 
            //console.log(key + ' = ' + value);
            a.push(key+"");
            }
          }
        )
        .catch(console.error);
        //console.log(a + ' a');
        if(a.length!=0){
          for(var x=0;x<a.length;x++){
            var msg= await message.channel.fetchMessage(a[x]);
            msg.delete();
          }
        await console.log(`${message.author.username} deleted some messages in ${message.channel.name}`)
        }
      }
      else(
        await message.channel.send("this comand only accepts 1 to 99")
      )
    } 
    else {
        await console.log(`${message.author.username} tried to use a command without privs`)
    }
  }
}
