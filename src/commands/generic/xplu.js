const level = require('../../helpers/level')
const {botCommandToDB} = require('../../helpers')

module.exports = {
  usage: '<@target>',
  description: 'Look up the XP of another user and be dissappointed in yourself.',
  process: async (msg) => {
    if (msg.mentions.users.first() !== undefined) {
      let target = (msg.mentions.users.first().id)
      await level.lookUpID(msg, target)
      console.log(target)
    } else {
        msg.channel.send('Mention a user to revoke.')
        .then(async function (newmsg) {
          await newmsg.react("❌");
          await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
          await msg.delete() 
        })
    }
  }
}