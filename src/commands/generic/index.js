const commands = {
  'lint': require('./lint'), //privileged only
  'xp': require('./xp'), //privileged only
  'revoke': require('./revoke'), //privileged, not implemented atm
  'reboot': require('./reboot'), //privileged only
  'enlist': require('./enlist'), // privileged only
  'backup': require('./backup'), //privs required
  'restore': require('./restore'), //privs required
  'seedusers': require('./seedusers'), //privs required
  'silent': require('./lockch'), //privs required
  'speak': require('./unlockch'), //privs required
  'clear': require('./clear'), //privs required
  'rp': require('./rp'),
  'xplu': require('./xplu'),
  'whoishere': require('./whoishere'),
  'propaganda': require('./propaganda'),
  'password': require('./password'),
  'info': require('./info'),
  'level': require('./level'),
  'map': require('./map'), 
  'ping': require('./ping'),
  'time': require('./time'),
  'userid': require('./userid'),
  'info': require('./info')
  }
  
  module.exports = commands