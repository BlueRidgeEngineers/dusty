const { permParser, checkRole } = require('../../helpers')
const knex = require('../../utils/database')

module.exports = {
    description: 'Unlock the Text channel, so regular peoples can write or add reaction again.',
    usage: '$speak',
    discrete: true,
    process: async msg => {
        if(await checkRole(msg, ['Elder', 'Admin', 'Moderator', 'Head Scribe', 'Moderator In Training'])){

            msg.channel.send("**Radio Silence is over!**\nFollow the previous orders!")

            let channel_id = msg.channel.id

            const channels = await knex
                .select('*')
                .from('ch_role_lock')
                .where({
                  'channel_id': channel_id})    
                
            channels.forEach(async function (channel) {
                if (channel.channel_id == channel_id) {
                    msg.channel.overwritePermissions(channel.role_id, await permParser(channel.role_int))
                    await knex('ch_role_lock').where('channel_id', channel_id).del().catch((err) => {console.log(err);});
                }
                else {

                }
            })

            console.log("'"+msg.channel.name+"' Unlocked.")
        }
    }
}