const botCommandToDB = require('../../helpers/botCommandToDB')

module.exports = {
  description: 'Check if the bot is online.',
  process: async msg => {
    await msg.channel.send(msg.author + ' pong!')
    .then(async function (newmsg) {
      await newmsg.react("❌");
      await botCommandToDB.saveBotCommandToDB(msg.author.id, newmsg.id);
      await msg.delete() 
    }) 
    await console.log(`${msg.author.username} pinged the bot`)
  }
}
